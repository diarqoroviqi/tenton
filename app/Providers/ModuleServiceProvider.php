<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    private $modules = [
        'Company',
        'Employee'
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindModules($this->modules);
    }

    /**
     * Binds module service and repository
     * @param $modules List of modules you want to bind.
     */
    private function bindModules( $modules )
    {
        foreach ( $modules as $module ) {
            $this->bindService($module);
            $this->bindRepository($module);
        }
    }

    /**
     * Bind service.
     * @param $module
     */
    private function bindService( $module )
    {
        $serviceContractPath = "App\\Modules\\{$module}\\Service\\Contract\\{$module}ServiceInterface";
        $servicePath = "App\\Modules\\{$module}\\Service\\{$module}Service";
        $this->app->singleton($serviceContractPath, $servicePath);
    }

    /**
     * Bind repository.
     * @param $module
     */
    private function bindRepository( $module )
    {
        $repoContractPath = "App\\Modules\\{$module}\\Repository\\Contract\\{$module}RepositoryInterface";
        $repoPath = "App\\Modules\\{$module}\\Repository\\{$module}Repository";
        $this->app->singleton($repoContractPath, $repoPath);
    }
}
