<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function view;

class AdminController extends Controller
{

    public function dashboard()
    {
        return view('admin.layouts.admin');
    }
}
