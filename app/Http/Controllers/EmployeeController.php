<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Modules\Company\Service\Contract\CompanyServiceInterface;
use App\Modules\Employee\Service\Contract\EmployeeServiceInterface;
use function dd;
use Illuminate\Http\Request;
use function view;

class EmployeeController extends Controller
{

    private $employeeService;
    /**
     * @var CompanyServiceInterface
     */
    private $companyService;

    public function __construct(
        EmployeeServiceInterface $employeeService,
        CompanyServiceInterface $companyService
    )
    {
        $this->employeeService = $employeeService;
        $this->companyService = $companyService;
    }

    public function create()
    {
        $companies = $this->companyService->getAll();
        return view('admin.employee.add')->with(['companies' => $companies]);
    }

    public function add(EmployeeRequest $request)
    {
        $this->employeeService->create($request->all());
        return redirect()->route('panel.employee.index')->with('success', [__('messages.employee_created')]);
    }

    public function index()
    {
        $employees = $this->employeeService->getAllWithEagerLoadingAndPagination('company', 10);
        return view('admin.employee.index')->with(['employees' => $employees]);
    }

    public function show($id)
    {
        $employee = $this->employeeService->getByIdWithEagerLoad($id, 'company');
        return view('admin.employee.show')->with(['employee' => $employee]);
    }

    public function edit($id)
    {
        $employee = $this->employeeService->getById($id);
        $companies = $this->companyService->getAll();
        return view('admin.employee.edit')->with(['employee' => $employee, 'companies' => $companies]);
    }

    public function update(EmployeeRequest $request, $id)
    {
        $employee = $this->employeeService->getById($id);
        $this->employeeService->update($employee, $request->all());
        return redirect()->route('panel.employee.show', ['id' => $employee->id])->with('success', [__('messages.employee_updated')]);
    }

    public function delete($id)
    {
        $this->employeeService->deleteById($id);
        return redirect()->route('panel.employee.index')->with('success', [__('messages.employee_deleted')]);
    }
}
