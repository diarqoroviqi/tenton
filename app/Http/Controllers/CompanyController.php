<?php

namespace App\Http\Controllers;

use App\Events\NewCompanyRegistered;
use App\Http\Requests\CompanyRequest;
use App\Modules\Company\Service\Contract\CompanyServiceInterface;
use function dd;
use function event;
use function storage_path;
use function view;

class CompanyController extends Controller
{
    private $companyService;

    public function __construct(CompanyServiceInterface $companyService)
    {

        $this->companyService = $companyService;
    }

    public function create()
    {
        return view('admin.company.add');
    }

    public function add(CompanyRequest $request)
    {
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $data = $this->companyService->saveImage($file, $request);
            $company = $this->companyService->create($data);
        } else {
            $company = $this->companyService->create($request->all());
        }

        event(new NewCompanyRegistered($company->name, $company->email));
        return redirect()->route('panel.company.index')->with('success', [__('messages.company_created')]);
    }

    public function index()
    {
        $companies = $this->companyService->getAllWithPagination(10);
        return view('admin.company.index')->with(['companies' => $companies]);
    }

    public function show($id)
    {
        $company = $this->companyService->getByIdWithEagerLoad($id, 'employees');
        return view('admin.company.show')->with(['company' => $company]);
    }

    public function edit($id)
    {
        $company = $this->companyService->getById($id);
        return view('admin.company.edit')->with(['company' => $company]);
    }

    public function update(CompanyRequest $request, $id)
    {
        $company = $this->companyService->getById($id);
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $data = $this->companyService->saveImage($file, $request);
            $this->companyService->update($company, $data);
        } else {
            $this->companyService->update($company, $request->all());
        }
        return redirect()->route('panel.company.show', ['id' => $company->id])->with('success', [__('messages.company_updated')]);
    }

    public function delete($id)
    {
        $this->companyService->deleteById($id);
        return redirect()->route('panel.company.index')->with('success', [__('messages.company_deleted')]);
    }

}



