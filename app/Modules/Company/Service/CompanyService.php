<?php


namespace App\Modules\Company\Service;


use App\Modules\Company\Repository\Contract\CompanyRepositoryInterface;
use App\Modules\Company\Service\Contract\CompanyServiceInterface;

class CompanyService implements CompanyServiceInterface
{

    private $companyRepository;

    public function __construct(CompanyRepositoryInterface $companyRepository)
    {

        $this->companyRepository = $companyRepository;
    }

    public function create($attributes)
    {
        return $this->companyRepository->create($attributes);
    }

    public function update($entity, $attributes)
    {
        $this->companyRepository->update($entity, $attributes);
    }

    public function deleteById($id)
    {
       $this->companyRepository->deleteById($id);
    }

    public function getById($id)
    {
        $company = $this->companyRepository->getById($id);
        return $company;
    }

    public function getAll()
    {
        $companies = $this->companyRepository->getAll();
        return $companies;
    }

    public function getAllWithPagination($pagination)
    {
        $companies = $this->companyRepository->getAllWithPagination($pagination);
        return $companies;
    }

    public function getByIdWithEagerLoad($id, $eagerLoad)
    {
        $company = $this->companyRepository->getByIdWithEagerLoad($id, $eagerLoad);
        return $company;
    }

    public function saveImage($file, $request)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $extension;
        $file->move(storage_path('app/public'), $filename);
        $request->request->add(['logo' => $filename]);

        return $data = [
            'name' => $request->name,
            'email' => $request->email,
            'website_url' => $request->website_url,
            'logo' => $filename,
        ];
    }

}