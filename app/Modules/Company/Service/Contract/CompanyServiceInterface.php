<?php


namespace App\Modules\Company\Service\Contract;


interface CompanyServiceInterface
{
    public function create($attributes);

    public function update($entity, $attributes);

    public function deleteById($id);

    public function getById($id);

    public function getAll();

    public function getAllWithPagination($pagination);

    public function getByIdWithEagerLoad($id, $eagerLoad);

    public function saveImage($file, $request);
}