<?php

namespace App\Modules\Company\Repository;

use App\Models\Company;
use App\Modules\Company\Repository\Contract\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{
    private $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($entity, $attributes)
    {
        $entity->update($attributes);

        return $entity;
    }

    public function deleteById($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getAll()
    {
        return $this->model->select(['*'])->get();
    }

    public function getAllWithPagination($pagination)
    {
        return $this->model->select(['*'])->orderBy('created_at', 'desc')->paginate($pagination);
    }

    public function getByIdWithEagerLoad($id, $eagerLoad)
    {
        return $this->model->with([$eagerLoad])->find($id);
    }
}