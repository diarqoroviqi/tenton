<?php


namespace App\Modules\Company\Repository\Contract;


interface CompanyRepositoryInterface
{

    public function create($attributes);

    public function update($entity, $attributes);

    public function deleteById($id);

    public function getById($id);

    public function getAllWithPagination($pagination);

    public function getAll();

    public function getByIdWithEagerLoad($id, $eagerLoad);
}