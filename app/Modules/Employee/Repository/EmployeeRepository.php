<?php

namespace App\Modules\Employee\Repository;

use App\Models\Employee;
use App\Modules\Employee\Repository\Contract\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    private $model;

    public function __construct(Employee $model)
    {
        $this->model = $model;
    }

    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($entity, $attributes)
    {
        $entity->update($attributes);

        return $entity;
    }

    public function deleteById($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getAll($columns = ['*'], $pagination)
    {
        return $this->model->select($columns)->paginate($pagination);
    }

    public function getAllWithEagerLoading($eagerLoad)
    {
        return $this->model->select(['*'])->with($eagerLoad)->get();
    }

    public function getAllWithEagerLoadingAndPagination($eagerLoad, $paginate)
    {
        return $this->model->select(['*'])->with($eagerLoad)->orderBy('created_at', 'desc')->paginate($paginate);
    }

    public function getByIdWithEagerLoad($id, $eagerLoad)
    {
        return $this->model->with([$eagerLoad])->find($id);
    }
}