<?php


namespace App\Modules\Employee\Repository\Contract;


interface EmployeeRepositoryInterface
{
    public function create($attributes);

    public function update($entity, $attributes);

    public function deleteById($id);

    public function getById($id);

    public function getAll($columns,$pagination);

    public function getAllWithEagerLoading($eagerLoad);

    public function getAllWithEagerLoadingAndPagination($eagerLoad, $paginate);

    public function getByIdWithEagerLoad($id, $eagerLoad);

}