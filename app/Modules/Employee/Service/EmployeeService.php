<?php


namespace App\Modules\Employee\Service;


use App\Modules\Employee\Repository\Contract\EmployeeRepositoryInterface;
use App\Modules\Employee\Service\Contract\EmployeeServiceInterface;

class EmployeeService implements EmployeeServiceInterface
{

    private $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function create($attributes)
    {
       $this->employeeRepository->create($attributes);
    }

    public function update($entity, $attributes)
    {
        $this->employeeRepository->update($entity, $attributes);
    }

    public function deleteById($id)
    {
        $this->employeeRepository->deleteById($id);
    }

    public function getById($id)
    {
        $employee = $this->employeeRepository->getById($id);
        return $employee;
    }

    public function getAll($columns, $pagination)
    {
        $employees = $this->employeeRepository->getAll($columns, $pagination);
        return $employees;
    }

    public function getAllWithEagerLoading($eagerLoad)
    {
        $employees = $this->employeeRepository->getAllWithEagerLoading($eagerLoad);
        return $employees;
    }

    public function getAllWithEagerLoadingAndPagination($eagerLoad, $paginate)
    {
        $employees = $this->employeeRepository->getAllWithEagerLoadingAndPagination($eagerLoad, $paginate);
        return $employees;
    }

    public function getByIdWithEagerLoad($id, $eagerLoad)
    {
        $employee = $this->employeeRepository->getByIdWithEagerLoad($id, $eagerLoad);
        return $employee;
    }
}