<?php

namespace App\Listeners;

use App\Events\NewCompanyRegistered;
use App\Mail\WelcomeMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewCompanyRegistered $event)
    {
        if (isset($event->companyEmail)) {
            Mail::to($event->companyEmail)->send(new WelcomeMail($event->companyName));
        }
    }
}
