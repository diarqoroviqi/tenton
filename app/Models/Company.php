<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Company extends Model
{

    protected $fillable = [
        'name',
        'email',
        'logo',
        'website_url'
    ];

    public function employees()
    {
        return $this->hasMany('App\Models\Employee', 'company_id', 'id');
    }

    public function getImagePath($fileName)
    {
        return Storage::url($fileName);
    }
}
