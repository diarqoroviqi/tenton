@extends('admin.layouts.guest')

@section('content')

    <p class="login-box-msg">{{ __('messages.login_to_start') }}</p>

    @include('partials.alerts')

    <form action="{{ route('login') }}" method="post">
        @csrf
        <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="{{ __('employee.email') }}" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="{{ __('employee.password') }}" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="remember" id="remember">{{ __('messages.remember_me') }}
                    </label>
                </div>
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-default btn-block btn-flat">
                    {{ __('auth.login')}}
                </button>
            </div>
        </div>
        <div class="form-group">

        </div>
    </form>



@endsection