<!-- jQuery 2.1.4 -->
<script src="{{ asset('assets/admin-panel/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('assets/admin-panel/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- Parsley JS -->
<script src="{{ asset('assets/admin-panel/js/parsley.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/admin-panel/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('assets/admin-panel/plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin-panel/js/app.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('assets/admin-panel/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- Fancybox -->
<script src="{{ asset('assets/admin-panel/js/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
<!-- Password Strength JS -->
<script src="{{ asset('assets/admin-panel/js/password-score.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin-panel/js/password-score-options.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin-panel/js/bootstrap-strength-meter.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/admin-panel/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/admin-panel/plugins/select2/select2.min.js') }}"></script>
<!-- Main JS-->
<script src="{{ asset('assets/admin-panel/js/main.js?v='.date('Ymd')) }}" type="text/javascript"></script>

</body>
</html>