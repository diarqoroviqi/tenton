<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/admin-panel/img/avatar5.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
            </div>

        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Main Menu</li>

            <li class="{{ (\Request::segment(2) == 'company') ? 'active' : ''  }}"><a href="{{ route('panel.company.index') }}">
                    <i class="fa fa-home"></i> <span>{{ __('company.companies') }}</span></a></li>
            <li class="{{ (\Request::segment(2) == 'employee') ? 'active' : ''  }}"><a href="{{ route('panel.employee.index') }}">
                    <i class="fa fa-users"></i> <span>{{ __('employee.employees') }}</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>