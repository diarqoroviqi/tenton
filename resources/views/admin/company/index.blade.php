@extends('admin.layouts.admin')

@section('content')
    @include('partials.alerts')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('company.companies') }}</h3>
            <a href="{{ route('panel.company.create') }}" class="pull-right btn btn-success">{{ __('company.add') }}</a>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('company.id') }}</th>
                            <th>{{ __('company.logo') }}</th>
                            <th>{{ __('company.name') }}</th>
                            <th>{{ __('company.email') }}</th>
                            <th>{{ __('company.website') }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($companies as $company)
                            <tr>
                                <td>
                                    <a href="{{ route('panel.company.show', ['id' => $company->id]) }}">#{{ $company->id }}</a>
                                </td>
                                <td class="logo-link">
                                    @if(isset($company->logo))
                                        <a href="{{ $company->getImagePath($company->logo) }}" target="_blank">
                                            <img class="logo-image" src="{{ $company->getImagePath($company->logo) }}"
                                                 alt="">
                                        </a>
                                    @else
                                        <img class="logo-image"
                                             src="{{ asset('assets/admin-panel/img/placeholder.png') }}" alt="">
                                    @endif
                                </td>
                                <td><a href="{{ route('panel.company.show', ['id' => $company->id]) }}">{{ ucfirst($company->name) }}</a></td>
                                <td>{{ isset($company->email) ? $company->email : '' }}</td>
                                <td>@if(isset($company->website_url))
                                        <a href="{{ $company->website_url }}">Website</a>
                                    @else
                                        {{ '' }}
                                    @endif
                                </td>
                                <td><a href="{{ route('panel.company.edit', ['id' => $company->id]) }}"
                                       class="btn btn-warning btn-edit btn-sm"><i class="fa fa-edit"></i></a></td>
                                <td>
                                    <form action="{{ route('panel.company.delete', ['id' => $company->id]) }}"
                                          id="delete">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-delete btn-sm"
                                                onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-remove"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $companies->links() }}
                </div>
            </div>
        </div>

    </div>


@endsection
