@extends('admin.layouts.admin')

@section('content')
    @include('partials.alerts')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('company.company') }}</h3>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form action="{{ route('panel.company.add') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{ __('company.name') }}:</label>
                            <input type="text" id="name" name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('company.email') }}:</label>
                            <input type="email" id="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="website_url">{{ __('company.website') }}:</label>
                            <input type="text" id="website_url" name="website_url" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="logo">{{ __('company.logo') }}:</label>
                            <input type="file" name="logo" id="logo" class="form-control">
                        </div>
                        <div class="form-group pull-right">
                            <input type="submit" class="btn btn-success" value="{{ __('company.add') }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection


