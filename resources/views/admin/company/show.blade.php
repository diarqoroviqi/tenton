@extends('admin.layouts.admin')

@section('content')
    @include('partials.alerts')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ ucfirst($company->name ) }}</h3>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    @if(isset($company->logo))
                        <img class="single-image" src="{{ $company->getImagePath($company->logo) }}" alt="">
                    @else
                        <img class="single-image" src="{{ asset('assets/admin-panel/img/placeholder.png') }}" alt="">
                    @endif
                </div>
                <div class="col-md-8">
                    <div class="card" style="padding-top: 15px;">
                        <p><strong>{{ __('company.email') }}:</strong> {{ isset($company->email) ?  $company->email : '' }}</p>
                        <p><strong>{{ __('company.website') }}:</strong> {{ isset($company->website_url) ?  $company->website_url : '' }}
                        </p>
                    </div>
                </div>
            </div>
            <hr>
            @if(count($company->employees) > 0)
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3>{{ __('messages.employees_of') }} {{ ucfirst($company->name) }}</h3>
                            </div>
                            <div class="panel-body">
                                <table class=" table table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('employee.name') }}:</th>
                                        <th>{{ __('employee.email') }}:</th>
                                        <th>{{ __('employee.phone') }}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($company->employees as $employee)
                                        <tr>
                                            <td>
                                                <a href="{{ route('panel.employee.show', ['id' => $employee->id]) }}">{{ $employee->fullName }}</a>
                                            </td>
                                            <td>{{ isset($employee->email) ? $employee->email: '' }}</td>
                                            <td>{{ isset($employee->phone) ? $employee->phone: '' }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>

    </div>
@endsection