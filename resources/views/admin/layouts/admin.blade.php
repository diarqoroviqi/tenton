@include('admin.partials.header')
@include('admin.partials.top-nav')
@include('admin.partials.left-nav')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if(isset($title))
            <h1>{{$title}}</h1>
        @else
            <h1><?= 'Dashboard' ?></h1>
        @endif
    </section>

    <!-- Main content -->
    <section class="content">

    @yield('content')

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@include('admin.partials.pre-footer')
@include('admin.partials.footer')