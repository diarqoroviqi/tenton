@include('admin.partials.header')
<div class="login-box">
    <div class="login-logo">
        <a href="#">{{ __('auth.login')}}</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">

    @yield('content')

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

@include('admin.partials.footer')