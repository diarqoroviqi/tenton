@extends('admin.layouts.admin')

@section('content')
    @include('partials.alerts')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ ucfirst($employee->fullName ) }}</h3>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        @if(isset($employee->company->logo))
                            <img src="{{ $employee->company->getImagePath($employee->company->logo) }}" style="width:100%">
                        @else
                            <img src="{{ asset('assets/admin-panel/img/placeholder.png') }}" style="width:100%">
                        @endif
                        <div class="container">
                            <h4><b>{{ ucfirst($employee->fullName) }}</b></h4>
                            <p><strong>{{ __('employee.email') }}:</strong> {{ isset($employee->email) ?  $employee->email : '' }}</p>
                            <p><strong>{{ __('employee.phone') }}:</strong> {{ isset($employee->phone) ?  $employee->phone : '' }}</p>
                            <p><strong>{{ __('company.company') }}:</strong> {{ ucfirst($employee->company->name) }}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection