@extends('admin.layouts.admin')

@section('content')
    @include('partials.alerts')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ 'Employees' }}</h3>
            <a href="{{ route('panel.employee.create') }}" class="pull-right btn btn-success">{{ __('employee.add') }}</a>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('employee.id') }}</th>
                            <th>{{ __('employee.name') }}</th>
                            <th>{{ __('employee.email') }}</th>
                            <th>{{ __('employee.phone') }}</th>
                            <th>{{ __('company.company') }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>
                                    <a href="{{ route('panel.employee.show', ['id' => $employee->id]) }}">#{{ $employee->id }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('panel.employee.show', ['id' => $employee->id]) }}">{{ $employee->fullName }}</a>
                                </td>
                                <td>{{ isset($employee->email) ? $employee->email : '' }}</td>
                                <td>{{ isset($employee->phone) ? $employee->phone : '' }}</td>
                                <td>{{ ucfirst($employee->company->name ) }}</td>
                                <td><a href="{{ route('panel.employee.edit', ['id' => $employee->id]) }}"
                                       class="btn btn-warning btn-edit btn-sm"><i class="fa fa-edit"></i></a></td>
                                <td>
                                    <form action="{{ route('panel.employee.delete', ['id' => $employee->id]) }}"
                                          id="delete">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-delete btn-sm"
                                                onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-remove"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $employees->links() }}
                </div>
            </div>
        </div>

    </div>


@endsection
