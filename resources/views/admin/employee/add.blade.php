@extends('admin.layouts.admin')

@section('content')
    @include('partials.alerts')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ 'Employee' }}</h3>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form action="{{ route('panel.employee.add') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="first_name">{{ __('employee.name') }}:</label>
                            <input type="text" id="first_name" name="first_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="last_name">{{ __('employee.last_name') }}:</label>
                            <input type="text" id="last_name" name="last_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('employee.email') }}:</label>
                            <input type="email" id="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('employee.phone') }}:</label>
                            <input type="text" id="phone" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="company">{{ __('company.company') }}:</label>
                            <select name="company_id" id="company" class="form-control">
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}">{{ ucfirst($company->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group pull-right">
                            <input type="submit" class="btn btn-success" value="{{ __('employee.add') }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


