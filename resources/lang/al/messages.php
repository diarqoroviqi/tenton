<?php

return [

    'employees_of' => 'Punetorët e',
    'remember_me' => 'Më Kujto',
    'login_to_start' => 'Kyçu të vazhdosh',

    'company_created' => 'Kompnaia u krijua me sukses',
    'company_deleted' => 'Kompania u fshijë me sukses',
    'company_updated' => 'Kompania u ndryshua me sukses',

    'employee_created' => 'Punëtori u krijua me sukses',
    'employee_updated' => 'Punëtori u ndryshua me sukses',
    'employee_deleted' => 'Punëtori u fshijë me sukses',

];