<?php

return [

    'id' => 'ID',
    'employees' => 'Punëtorët',
    'phone' => 'Telefoni',
    'name' => 'Emri',
    'last_name' => 'Mbiemri',
    'email' => 'Emaili',
    'add' => 'Shto',
    'update' => 'Ndrysho',
    'password' => 'Fjalëkalimi'
];