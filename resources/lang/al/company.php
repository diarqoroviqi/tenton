<?php

return [

    'id' => 'ID',
    'company' => 'Kompania',
    'companies' => 'Kompanitë',
    'name' => 'Emri',
    'last_name' => 'Mbiemri',
    'email' => 'Emaili',
    'logo' => 'Logo',
    'website' => 'Faqja',
    'add' => 'Shto',
    'update' => 'Ndrysho'

];