<?php

Route::group(['prefix' => 'employee'], function () {

    Route::get('/create', [
        'as' => 'panel.employee.create',
        'uses' => 'EmployeeController@create',
        'middleware' => ['auth']
    ]);

    Route::post('/create', [
        'as' => 'panel.employee.add',
        'uses' => 'EmployeeController@add',
        'middleware' => ['auth']
    ]);

    Route::get('/', [
        'as' => 'panel.employee.index',
        'uses' => 'EmployeeController@index',
        'middleware' => ['auth']
    ]);

    Route::get('/{id}', [
        'as' => 'panel.employee.show',
        'uses' => 'EmployeeController@show',
        'middleware' => ['auth']
    ]);

    Route::get('/{id}/edit', [
        'as' => 'panel.employee.edit',
        'uses' => 'EmployeeController@edit',
        'middleware' => ['auth']
    ]);

    Route::post('/{id}/edit', [
        'as' => 'panel.employee.update',
        'uses' => 'EmployeeController@update',
        'middleware' => ['auth']
    ]);

    Route::get('/{id}/delete', [
        'as' => 'panel.employee.delete',
        'uses' => 'EmployeeController@delete',
        'middleware' => ['auth']
    ]);

});