<?php

Route::group(['prefix' => 'company'], function () {


    Route::get('/', [
        'as' => 'panel.company.index',
        'uses' => 'CompanyController@index',
        'middleware' => ['auth']
    ]);

    Route::get('/create', [
        'as' => 'panel.company.create',
        'uses' => 'CompanyController@create',
        'middleware' => ['auth']
    ]);

    Route::get('/{id}', [
        'as' => 'panel.company.show',
        'uses' => 'CompanyController@show',
        'middleware' => ['auth']
    ]);


    Route::post('/create', [
        'as' => 'panel.company.add',
        'uses' => 'CompanyController@add',
        'middleware' => ['auth']
    ]);

    Route::get('/{id}/edit', [
        'as' => 'panel.company.edit',
        'uses' => 'CompanyController@edit',
        'middleware' => ['auth']
    ]);

    Route::post('/{id}/update', [
        'as' => 'panel.company.update',
        'uses' => 'CompanyController@update',
        'middleware' => ['auth']
    ]);

    Route::get('/{id}/delete', [
        'as' => 'panel.company.delete',
        'uses' => 'CompanyController@delete',
        'middleware' => ['auth']
    ]);

});
