<?php

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [
        'as' => 'panel.admin.dashboard',
        'uses' => 'AdminController@dashboard',
        'middleware' => ['auth']
    ]);

});