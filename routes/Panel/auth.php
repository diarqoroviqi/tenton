<?php

Route::group(['prefix' => 'auth'], function () {
    Route::get('/login', [
        'as' => 'getLogin',
        'uses' => 'Auth\LoginController@showLoginForm'
    ]);

    Route::post('/login', [
        'as' => 'login',
        'uses' => 'Auth\LoginController@login',
    ]);

    Route::get('/logout', [
        'as' => 'logout',
        'uses' => 'Auth\LoginController@logout',
        'middleware' => ['auth']
    ]);

});